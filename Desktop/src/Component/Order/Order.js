import React from "react";
import { useDispatch } from "react-redux";
import { deleteOrders } from "../../store/actions/ordersAction";
import "./Order.css";

const Order = (props) => {
  const dispatch = useDispatch();

  const completeOrder = (id) => {
    dispatch(deleteOrders(id));
  };

  return (
    <div className="Order">
      <div>
        {props.order}
        <p>Delivery :150KGS</p>
      </div>

      <div>
        <p>Total:{props.totalPrice}</p>
        <button onClick={() => completeOrder(props.id)}>Complete Order</button>
      </div>
    </div>
  );
};

export default Order;
