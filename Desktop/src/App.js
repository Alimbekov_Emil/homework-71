import { Route, Switch } from "react-router-dom";
import Layout from "./Component/Layout/Layout";
import AddPage from "./Container/AddPage/AddPage";
import AdminPage from "./Container/AdminPage/AdminPage";
import EditPage from "./Container/EditPage/EditPage";
import Orders from "./Container/Orders/Orders";

const App = () => (
  <Layout>
    <Switch>
      <Route path="/" exact component={AdminPage} />
      <Route path="/edit/:id" component={EditPage} />
      <Route path="/add" component={AddPage} />
      <Route path="/orders" exact component={Orders} />
    </Switch>
  </Layout>
);

export default App;
