import axiosDishes from "../../axiosDishes";
import { fetchDishes } from "./dishesAction";

export const FETCH_ORDERS_REQUEST = "FETCH_ORDERS_REQUEST";
export const FETCH_ORDERS_SUCCESS = "FETCH_ORDERS_SUCCESS";
export const FETCH_ORDERS_FAILURE = "FETCH_ORDERS_FAILURE";

export const DELETE_ORDERS_REQUEST = "DELETE_ORDERS_REQUEST";
export const DELETE_ORDERS_SUCCESS = "DELETE_ORDERS_SUCCESS";
export const DELETE_ORDERS_FAILURE = "DELETE_ORDERS_FAILURE";

export const fetchOrdersRequest = () => ({ type: FETCH_ORDERS_REQUEST });
export const fetchOrdersSuccess = (orders) => ({
  type: FETCH_ORDERS_SUCCESS,
  orders,
});
export const fetchOrdersFailure = (error) => ({
  type: FETCH_ORDERS_FAILURE,
  error,
});

export const deleteOrdersRequest = () => ({ type: DELETE_ORDERS_REQUEST });
export const deleteOrdersSucess = () => ({
  type: DELETE_ORDERS_SUCCESS,
});
export const deleteOrdersFailure = (error) => ({
  type: DELETE_ORDERS_FAILURE,
  error,
});

export const fetchOrders = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchOrdersRequest());
      await dispatch(fetchDishes());
      const response = await axiosDishes.get("orders.json");
      dispatch(fetchOrdersSuccess(response.data));
    } catch (e) {
      dispatch(fetchOrdersFailure(e));
    }
  };
};

export const deleteOrders = (id) => {
  return async (dispatch) => {
    try {
      dispatch(deleteOrdersRequest());
      await axiosDishes.delete("orders/" + id + ".json");
      dispatch(deleteOrdersSucess());
      dispatch(fetchOrders());
    } catch (e) {
      dispatch(deleteOrdersFailure(e));
    }
  };
};
