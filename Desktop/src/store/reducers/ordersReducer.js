import { FETCH_DISHES_REQUEST } from "../actions/dishesAction";
import {
  DELETE_ORDERS_FAILURE,
  DELETE_ORDERS_REQUEST,
  DELETE_ORDERS_SUCCESS,
  FETCH_ORDERS_REQUEST,
  FETCH_ORDERS_SUCCESS,
} from "../actions/ordersAction";

const initialState = {
  orders: [],
  dishes: [],
  sum: 150,
  loading: false,
  erorr: false,
};

const ordersReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ORDERS_REQUEST:
      return { ...state, loading: true };
    case FETCH_ORDERS_SUCCESS:
      return { ...state, loading: false, orders: action.orders };
    case FETCH_DISHES_REQUEST:
      return { ...state, loading: false, error: action.error };
    case DELETE_ORDERS_REQUEST:
      return { ...state, loading: true };
    case DELETE_ORDERS_SUCCESS:
      return { ...state, loading: false };
    case DELETE_ORDERS_FAILURE:
      return { ...state, loading: false, error: action.error };

    default:
      return { ...state };
  }
};

export default ordersReducer;
