import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Spinner from "../../Component/UI/Spinner/Spinner";
import { fetchDishe, putDishe } from "../../store/actions/dishesAction";
import "./EditPage.css";

const EditPage = (props) => {
  const dispatch = useDispatch();
  const dishe = useSelector((state) => state.dishes.dishe);
  const loading = useSelector((state) => state.dishes.loading);

  const [dishes, setDishes] = useState({
    name: "",
    price: "",
    image: "",
  });

  useEffect(() => {
    dispatch(fetchDishe(props.match.params.id));
  }, [dispatch, props.match.params.id]);

  useEffect(() => {
    setDishes(dishe);
  }, [dishe]);

  const dishesDataChanged = (event) => {
    const { name, value } = event.target;

    setDishes((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const editNewProduct = (e) => {
    e.preventDefault();
    dispatch(putDishe(props.match.params.id, dishes));
    props.history.push("/");
  };

  return (
    <div className="FieldForm">
      <h2>Edit Dishes</h2>
      {loading === true ? <Spinner /> : null}

      <form onSubmit={editNewProduct}>
        <input
          onChange={dishesDataChanged}
          className="Input"
          name="name"
          value={dishes.name}
          placeholder="Name"
        />
        <input
          onChange={dishesDataChanged}
          className="Input"
          name="price"
          value={dishes.price}
          placeholder="Price"
        />
        <input
          onChange={dishesDataChanged}
          className="Input"
          name="image"
          value={dishes.image}
          placeholder="Image"
        />
        <button type="submit">Edit</button>
      </form>
    </div>
  );
};

export default EditPage;
