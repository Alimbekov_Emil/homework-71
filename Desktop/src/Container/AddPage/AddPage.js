import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Spinner from "../../Component/UI/Spinner/Spinner";
import { fetchDishes, postDishes } from "../../store/actions/dishesAction";
import "./AddPage.css";

const AddPage = (props) => {
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.dishes.loading);

  const [dishes, setDishes] = useState({
    name: "",
    price: "",
    image: "",
  });

  const dishesDataChanged = (event) => {
    const { name, value } = event.target;

    setDishes((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const createNewProduct = (e) => {
    e.preventDefault();

    dispatch(postDishes(dishes));
    setDishes({ name: "", price: "", image: "" });
    props.history.push("/");
    dispatch(fetchDishes());
  };

  return (
    <div className="FieldForm">
      {loading === true ? <Spinner /> : null}
      <h2>Create New Dishes</h2>
      <form onSubmit={createNewProduct}>
        <input
          onChange={dishesDataChanged}
          className="Input"
          name="name"
          value={dishes.name}
          placeholder="Name"
        />
        <input
          onChange={dishesDataChanged}
          className="Input"
          name="price"
          value={dishes.price}
          placeholder="Price"
          type="number"
        />
        <input
          onChange={dishesDataChanged}
          className="Input"
          name="image"
          value={dishes.image}
          placeholder="Image"
        />
        <button>Add</button>
      </form>
    </div>
  );
};

export default AddPage;
